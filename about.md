---
layout: page
title: About
permalink: /about/
---

This is a site showing off projects made by me for university. I'm 
currently doing Bachelor of Engineering (Hons) majoring in mechatronics, 
studying at Macquarie University. I'll mostly be showing off cool things 
I've made for uni after they've been submitted, as 
well as any cool side projects I've been working on.
