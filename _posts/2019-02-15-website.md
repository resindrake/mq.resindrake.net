---
layout: post
title:  "A Website for All My Projects"
date:   2019-02-15 18:03:10 +1100
---
I've finally gotten around to laying this out correctly after setting up DDNS to deal with my home's IP constantly changing. On top of that, I set up Jekyll correctly this time, so posting stuff here will be a lot easier. Now I 
have a properly functioning website that won't go down 50% of the time that will actually look nice!  
I'm planning on posting projects here that I work on for uni. Not the boring essay stuff of course, just anything fun that involves building something. I might also post some side-projects I do in my free time.  
Hopefully I'll have a bunch of cool stuff to share here in the future!
